﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SwaggerAuth.Utils;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace SwaggerAuth
{
    public class Startup
    {
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration["DbContextSettings:ConnectionString"];

            //Using the Npgsql EF Core Provider (http://www.npgsql.org/efcore/index.html)
            services.AddDbContext<ApiDbContext>(opts => opts.UseNpgsql(connectionString));

            services.AddSwaggerDocumentation();

            services.AddIdentity<ApplicationUser, IdentityRole>(
                options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 3;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                }
                )
            .AddEntityFrameworkStores<ApiDbContext>()
            .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;

                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["TokenAuthentication:Issuer"],
                    ValidAudience = Configuration["TokenAuthentication:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["TokenAuthentication:SecretKey"]))
                };
                cfg.Events = new JwtBearerEvents
                {
                    OnTokenValidated = OnTokenValidated
                };
            });

            services.AddMvc();
            services.AddAuthorization();
        }

        private static async Task OnTokenValidated(TokenValidatedContext context)
        {
            try
            {
                var principalClaim = context.Principal.Claims.Where(x => x.Type == System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti);
                     
                var userManager = context.HttpContext.RequestServices.GetService<UserManager<ApplicationUser>>();

                var user = await userManager.FindByIdAsync(principalClaim.SingleOrDefault().Value);

                var identityClaims = await userManager.GetClaimsAsync(user);

                if (identityClaims.Count() > 0)
                {
                    //This is going to prevent unsigned user from using valid access_token
                    var loginUser = identityClaims.Where(x => x.Type == "LoginUser").SingleOrDefault().Value;
                    if (loginUser == "False")
                        context.Fail("the user is not logged in any longer");
                    else
                    {
                        //This is going to check if refresh token is expired and issue a new one if required
                        var validTimeOfRefreshToken = identityClaims.Where(x => x.Type == "RefreshTokenExpTime" ).SingleOrDefault().Value;
                   
                        if (DateTime.Parse(validTimeOfRefreshToken) < DateTime.UtcNow)
                        {
                            var token = GetLoginToken.Execute(user, context.HttpContext);
                            var serializerSettings = new JsonSerializerSettings
                            {
                                Formatting = Formatting.Indented
                            };
                            context.Response.ContentType = "application/json";
                            await context.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(token, serializerSettings));
                        }
                    } 
                }
            }
            catch (Exception ex) { }

            await Task.FromResult(0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwaggerDocumentation();
            }
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
