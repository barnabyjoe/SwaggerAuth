﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwaggerAuth.Dtos
{
    // User Data Transfer Object
    public class UserDto
    {
        public string login { get; set; }
        public string pass { get; set; }
    }
}
