﻿using Microsoft.IdentityModel.Tokens;
using System;

namespace SwaggerAuth.TokenProvider
{
    public class TokenProviderOptions
    {

        /// <summary>
        ///  The Issuer (iss) claim for generated tokens.
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// The Audience (aud) claim for the generated tokens.
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// The expiration time for the generated tokens.
        /// </summary>
        public TimeSpan Expiration { get; set; } 

        /// <summary>
        /// The refresh time for the generated tokens.
        /// </summary>
        public TimeSpan Refresh { get; set; } 

        /// <summary>
        /// The signing key to use when generating tokens.
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }
    }
}
