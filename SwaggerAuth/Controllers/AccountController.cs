﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SwaggerAuth.Dtos;
using Microsoft.EntityFrameworkCore;
using SwaggerAuth.Utils;

namespace SwaggerAuth.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IPasswordHasher<ApplicationUser> _hasher;

        public AccountController(
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           IPasswordHasher<ApplicationUser> hasher)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _hasher = hasher;
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]UserDto model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.Users.SingleAsync(i => i.UserName == model.login);
                if (_hasher.VerifyHashedPassword(user, user.PasswordHash, model.pass) ==
                    PasswordVerificationResult.Success)
                {
                    var response = GetLoginToken.Execute(user, HttpContext);
                    var identityClaims = await _userManager.GetClaimsAsync(user);
                    if (identityClaims.Count() > 0)
                    {
                        await _userManager.ReplaceClaimAsync(user, identityClaims.Where(x => x.Type == "LoginUser").SingleOrDefault(), new System.Security.Claims.Claim("LoginUser", "True"));
                        await _userManager.ReplaceClaimAsync(user, identityClaims.Where(x => x.Type == "RefreshTokenExpTime").SingleOrDefault(), new System.Security.Claims.Claim("RefreshTokenExpTime", response.refresh_token_expires_in.ToLongDateString()));
                    }
                    else
                    {
                        await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("LoginUser", "True"));
                        await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("RefreshTokenExpTime", response.refresh_token_expires_in.ToLongDateString()));
                    }

                    return Ok(response);
                }
            }
            return BadRequest("Wrong login or password");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]UserDto model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.login};
                var result = await _userManager.CreateAsync(user, model.pass);
                if (result.Succeeded)
                {                    
                    return await this.Login(model);
                }
            }
            return BadRequest("Wrong login or password");
        }

        [HttpPost]
        //optional parameter obj (null) is only for the sake of swagger to display Authorization header field 
        public async Task<IActionResult> RefreshToken(object obj = null)
        {
            try
            {
                var clm = User.Claims.Where(x => x.Type == System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(clm.SingleOrDefault().Value);
                if (user != null)
                {
                    var response = GetLoginToken.Execute(user, HttpContext);
                    return Ok(response);
                }
            }
            catch (Exception ex) { }


            return BadRequest();
        }

        [HttpDelete]
        //optional parameter obj (null) is only for the sake of swagger to display Authorization header field 
        public async Task<IActionResult> Logout(object obj = null)
        {
            try
            {
                var principalClaim = User.Claims.Where(x => x.Type == System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(principalClaim.SingleOrDefault().Value);

                var identityClaims = await _userManager.GetClaimsAsync(user);

                if (identityClaims.Count() > 0)
                    await _userManager.ReplaceClaimAsync(user, identityClaims.Where(x => x.Type == "LoginUser").SingleOrDefault(), new System.Security.Claims.Claim("LoginUser", "False"));
            }
            catch (Exception ex) { }

            return Ok("User is unsigned");
        }
    }
}