﻿namespace SwaggerAuth.Utils
{
    public class LoginResponse
    {
        public string access_token { get; set; }
        public System.DateTime expires_in { get; set; }
        public string refresh_token { get; set; }
        public System.DateTime refresh_token_expires_in { get; set; }
        public string userName { get; set; }
    }
}
